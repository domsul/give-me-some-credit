import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.model_selection import KFold
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from evolutionary_search import EvolutionaryAlgorithmSearchCV
from imblearn.over_sampling import SMOTE


def check_na(df):
    """
    Find columns with NA values in DataFrame.
    :param df: DataFrame
    :return: list of column names with NA values
    """
    na_cols = []
    for c in df.columns:
        if df[c].isnull().values.any():
            na_cols.append(c)
            continue
    return na_cols


def fill_na(df, plots=False):
    """
    Fill NA with the median of the specific column.
    :param df: DataFrame
    :param plots: flag, True if you want to have plots
    :return: filled DataFrame
    """
    na_cols = check_na(df)
    for c in na_cols:
        if plots:
            x_train[c].plot()
            plt.show()
        df[c].fillna(df[c].mean(), inplace=True)


def find_best_params(xtrain, ytrain, classifier, param_grid):
    """
    Find the best hyperparameters using a genetic algorithm.
    :param xtrain: train set
    :param ytrain: train set labels
    :param classifier: classifier to optimize
    :param param_grid: dict of parameters
    :return: the best parameters and accuracy score
    """
    cv = KFold(n_splits=10)
    search = EvolutionaryAlgorithmSearchCV(estimator=classifier,
                                           params=param_grid,
                                           cv=cv,
                                           population_size=100,
                                           gene_mutation_prob=0.2,
                                           gene_crossover_prob=0.8,
                                           tournament_size=20,
                                           generations_number=10)
    search.fit(xtrain, ytrain)
    results = pd.DataFrame(search.cv_results_).sort_values("mean_test_score", ascending=False)
    print(results[['mean_test_score', 'min_test_score', 'max_test_score']])
    print(results['params'][0])


def find_best_classifier(x_train, y_train, name=''):
    """
    Finding the best classifier was cut into a separate feature, because it was used only once.
    :param x_train: train set
    :param y_train: train set labels
    :param name: name of classifier
    :return: the best classifier with its parameters
    """
    clf = None
    paramgrid = {}
    if name == 'dt':
        clf = DecisionTreeClassifier()
        paramgrid = {"criterion": ["gini", "entropy"],
                     "splitter": ["best", "random"],
                     "min_samples_split": np.arange(2, 1000, 1),
                     "min_samples_leaf": np.arange(1, 1000, 1),
                     "min_weight_fraction_leaf": np.arange(0, 0.5, 0.01)}
    elif name == 'rf':
        clf = RandomForestClassifier()
        paramgrid = {"n_estimators": np.arange(1, 100, 1),
                     "criterion": ["gini", "entropy"],
                     "min_samples_split": np.arange(2, 100, 1),
                     "min_samples_leaf": np.arange(1, 100, 1),
                     "min_weight_fraction_leaf": np.arange(0, 0.5, 0.01),
                     "n_jobs": [-1],
                     "verbose": np.arange(0, 100, 1),
                     "class_weight": ["balanced", "balanced_subsample", None]}
    elif name == 'lr':
        clf = LogisticRegression()
        paramgrid = {"penalty": ["l2"],
                     "C": np.arange(0.1, 1.1, 0.1),
                     "solver": ['lbfgs'],
                     "max_iter": np.array([10000])}
    find_best_params(x_train,y_train,clf,paramgrid)


def learning_and_predicting(x_train, y_train, x_test, clf):
    """
    Create a model and predict probabilities.
    :param x_train: train set
    :param y_train: train set labels
    :param x_test: test set
    :param clf: classifier
    :return: predictions
    """
    boost_clf = AdaBoostClassifier(base_estimator=clf)
    boost_clf.fit(x_train, y_train)
    # predicting probabilities
    y_pred = boost_clf.predict_proba(x_test)
    print('Accuracy: ', boost_clf.score(x_train, y_train))
    return y_pred


if __name__ == '__main__':
    # load dataset and create train set with labels and test set
    x_train = pd.read_csv('cs-training.csv', index_col=0)
    x_test = pd.read_csv('cs-test.csv')
    y_train = x_train['SeriousDlqin2yrs']
    indexes = x_test['Unnamed: 0']  # this is used during writing probabilities to the file

    # fill missing values
    fill_na(x_train)
    fill_na(x_test)

    # delete unnecessary features from the sets
    del x_train['SeriousDlqin2yrs']
    del x_test['SeriousDlqin2yrs']
    del x_test['Unnamed: 0']

    # hyperparameters optimization from the part of full dataset
    # find_best_classifier(x_train[:15000],y_train[:15000],'rf')   # used once!

    # oversampling
    x_train, y_train = SMOTE().fit_resample(x_train, y_train)

    # the best classifier with its hyperparameters (from find_best_classifier)
    params = {'n_estimators': 21,
              'criterion': 'gini',
              'min_samples_split': 55,
              'min_samples_leaf': 64,
              'min_weight_fraction_leaf': 0.06,
              'n_jobs': -1,
              'verbose': 72,
              'class_weight': None}
    clf = RandomForestClassifier(**params)

    # create a model and predict probabilities
    y_pred = learning_and_predicting(x_train, y_train, x_test, clf)

    # save to .csv file
    entry = pd.DataFrame(y_pred, columns=['Id', 'Probability'])
    entry['Id'] = indexes
    entry.to_csv('Entry.csv', index=None)
